# Install

```console
mkdir mkdocs-utery
cd mkdocs-utery/
virtualenv venv -p python3
. venv/bin/activate
pip install -r requirements.txt
mkdocs serve
```
